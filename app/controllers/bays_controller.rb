class BaysController < ApplicationController
  before_action :set_bay, only: [:show, :update, :destroy]

  # GET /bays
  def index
    @bays = Bay.all

    render json: @bays
  end

  # GET /bays/1
  def show
    render json: @bay
  end

  # POST /bays
  def create
    @bay = Bay.new(bay_params)

    if @bay.save
      render json: @bay, status: :created, location: @bay
    else
      render json: @bay.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /bays/1
  def update
    if @bay.update(bay_params)
      render json: @bay
    else
      render json: @bay.errors, status: :unprocessable_entity
    end
  end

  # DELETE /bays/1
  def destroy
    @bay.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bay
      @bay = Bay.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def bay_params
      params.fetch(:bay, {})
    end
end
