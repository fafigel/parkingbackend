Rails.application.routes.draw do
  resources :bookings
  resources :cars
  resources :categories
  resources :customers
  resources :bays
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
