# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "bays", primary_key: "bayNumber", id: :integer, default: nil, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "category", limit: 20
    t.string "allocated", limit: 11
    t.index ["category"], name: "category"
  end

  create_table "bookings", primary_key: "bookingId", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "customer", limit: 20, null: false
    t.string "car", limit: 7, null: false
    t.integer "bayNumber", null: false
    t.string "parkingDuration", limit: 6, null: false
    t.integer "hoursParked", default: 24
    t.timestamp "checkInTime", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["bayNumber"], name: "bayNumber"
    t.index ["car"], name: "car"
    t.index ["customer"], name: "customer"
  end

  create_table "cars", primary_key: "regNumber", id: :string, limit: 7, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "make", limit: 30, null: false
    t.string "model", limit: 40, null: false
  end

  create_table "categories", primary_key: "categoryName", id: :string, limit: 20, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.decimal "hourlyPrice", precision: 5, scale: 2, null: false
    t.decimal "dailyPrice", precision: 5, scale: 2, null: false
  end

  create_table "customers", primary_key: "licenseNumber", id: :string, limit: 20, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "firstName", limit: 20, null: false
    t.string "surname", limit: 40, null: false
    t.string "gender", limit: 6, null: false
  end

  add_foreign_key "bays", "categories", column: "category", primary_key: "categoryName", name: "bays_ibfk_1"
  add_foreign_key "bookings", "bays", column: "bayNumber", primary_key: "bayNumber", name: "bookings_ibfk_3"
  add_foreign_key "bookings", "cars", column: "car", primary_key: "regNumber", name: "bookings_ibfk_2"
  add_foreign_key "bookings", "customers", column: "customer", primary_key: "licenseNumber", name: "bookings_ibfk_1"
end
